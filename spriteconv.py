#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#Using native format of https://www.spritemate.com/

import argparse
import os
import json

def GetByte(bits):
	byte=0
	for bit in bits:
		byte=(byte << 1)|bit
	return byte

def SaveSpriteLine(outhandle, line):
	for block in range(3):
		blockpixels=line[block*8:(block+1)*8]
		outhandle.write(bytes([GetByte(blockpixels)]))

def SaveSprite(outhandle, sprite):
	for line in sprite['pixels']:
		SaveSpriteLine(outhandle, line)

def SpriteConv(infile, outfile):
	out=open(outfile, 'wb')
	with open(infile, 'r') as inhandle:
		j=json.loads(inhandle.read())
		for spr in j['sprites']:
			print("Adding sprite", spr['name'])
			SaveSprite(out, spr)
			#Write unused extra byte, to keep sprites aligned to 64 bytes.
			out.write(bytes([0]))
			

def main(argv=None):
	parser=argparse.ArgumentParser(description='Leech kemono.party.')
	parser.add_argument('-i', '--input', required = True, dest='input', action='store', help='Input filename')
	parser.add_argument('-o', '--outfile', required = True, dest='output', action='store', help='Output file')
	args=parser.parse_args()
	SpriteConv(args.input, args.output)

if __name__ == "__main__":
	main()
