;memory map
;Bank 1, add $4000 for absolute address
;As seen by:
;        VIC2         CPU
;sprites $3000-$33FF ($7000-)
;screen  $3400-$37FF ($7400-)
;charset $3800-$3FFF ($7800-)
;screen config %1101111x => 222

a=a+1
ifa=1thenload"data",8,1
;basic end $70 => 112
;CLR kaskyn jalkeen KAIKKI (vanhat) muuttujat katoaa
poke53280,0:poke55,0:poke56,112:clr:poke53272,222:poke56576,2:poke648,116
fori=0to255:poke29696+i,i:next
poke53248,100:poke53249,100:poke53264,0:poke53269,1:poke53287,2:
;Sprite pointer for first sprite - relative to screen at $7400 (as seen by CPU) ($77F8, 30712)
;First sprite at $3000 (as seen by VIC2) => $3000/64=192
poke30712,192
