C1541 = c1541
PETCAT = petcat

SOURCE = basic.bas
EXEFILE = basic.prg
DISKFILENAME = basic.d64
DISKNAME = basic
ID = 20

all: d64

sprites: mysprites.spm
	./spriteconv.py -i mysprites.spm -o data/sprites

datafile: sprites
	#echo -n -e "\x42\x43\x44" > data/data
	/usr/bin/printf "%b" '\x00\x70' > data/data
	cat data/sprites >> data/data
	cat data/screen >> data/data
	/usr/bin/printf "%b" '\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' >> data/data
	cat data/charset >> data/data

basic:
	$(PETCAT) -w2 -o $(EXEFILE) -- $(SOURCE)

d64: basic datafile
	$(C1541) -format $(DISKNAME),$(ID) d64 $(DISKFILENAME)
	$(C1541) -attach $(DISKFILENAME) -write $(EXEFILE)
	$(C1541) -attach $(DISKFILENAME) -write data/data
	$(C1541) -attach $(DISKFILENAME) -list

run: d64
	x64 $(DISKFILENAME)

clean:
	rm $(EXEFILE) $(DISKFILENAME)
